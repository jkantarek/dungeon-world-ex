defmodule DungeonWorldGenerators do
  @moduledoc """
  Documentation for DungeonWorldGenerators.
  """

  @doc """
  Hello world.

  ## Examples

      iex> DungeonWorldGenerators.hello
      :world

  """
  def hello do
    :world
  end
end
